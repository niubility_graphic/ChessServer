﻿using System;
using ChessServer.Log;

namespace ChessServer.Net {
    public class DataMessage {
        public DataMessage ( byte [] msg ) {
            if ( msg == null || msg.Length == 0 ) {
                opcode = OpCode.Close;
            }
            opcode = OpCode.Binary;
            if ( msg.Length <= 4 ) {
                OurDebug.LogError ( "DataMessage too short small than 4" );
                return;
            }
            _msgid = BitConverter.ToInt32 ( msg, 0 );
            _data = new byte [msg.Length - 4];
            Array.Copy ( msg, 4, _data, 0, msg.Length - 4 );
        }
        private byte [] _data;

        public byte [] data { get {
                return _data; }
        }
        private int _msgid;
        public int msgid { get { return _msgid; } }

        public int opcode;
    }
}