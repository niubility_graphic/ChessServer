﻿

using System;

namespace ChessServer.Net {
    public class MessageStructure {
        public byte [] Data;

        public MessageStructure ( int msgid, byte [] msg ) {
            var idbytes = BitConverter.GetBytes ( msgid );
            Array.Copy ( idbytes, 0, Data, 0, idbytes.Length );
            Array.Copy ( msg, 0, Data, idbytes.Length, msg.Length );
        }
    }
}