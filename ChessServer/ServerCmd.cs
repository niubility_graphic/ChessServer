﻿using System;
using ChessServer.Log;

namespace ChessServer {
    public static class ServerCmd {
        public static int Run () {
            while ( Start.instance.isrunning () ) {
                var cmd = Console.ReadLine ();
                prcess ( cmd );
            }
            return 0;
        }

        private static void prcess ( string cmd ) {
            // 
            if ( cmd == "stop" ) {
                Start.instance.stop ();
            }
            else {
                OurDebug.Log ( "unhandled cmd" );
            }
        }
    }
}