﻿namespace ChessServer {
    internal interface IStart {
        int init ();
        int run ();
        int stop ();
        bool isrunning ();
    }
}